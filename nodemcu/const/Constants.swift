//
//  Constants.swift
//  qardan
//
//  Created by Mufaddal Gulshan on 18/11/18.
//  Copyright © 2018 Saif Limited. All rights reserved.
//

import Foundation

class Constants {
    public static let KEY_HOTSPOT_SSID = "KEY_HOTSPOT_SSID"
    public static let KEY_SETTINGS = "KEY_SETTINGS"
    public static let KEY_MQTT_USERNAME = "KEY_MQTT_USERNAME"
    public static let KEY_MQTT_PASSWORD = "KEY_MQTT_PASSWORD"
    public static let KEY_MQTT_HOST = "KEY_MQTT_HOST"
    public static let KEY_MQTT_PORT = "KEY_MQTT_PORT"
    public static let SSID = "ESPap"
    public static let PASSWORD = "thereisnospoon"
    public static let CONNECT = "Connect to NodeMCU AP"
    public static let CONNECTING = "Connecting to NodeMCU AP"
    public static let CONNECTED = "Connected to NodeMCU AP"
}
