//
//  ViewController.swift
//  nodemcu
//
//  Created by Mufaddal Gulshan on 05/03/19.
//  Copyright © 2019 Saif Limited. All rights reserved.
//

import UIKit
import NetworkExtension
import SystemConfiguration.CaptiveNetwork
import MQTTClient
import TTGSnackbar

class ViewController: UIViewController {

    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var btnMqtt: UIButton!

    var connected: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.

    }

    override func viewWillAppear(_ animated: Bool) {
        // show delete button only if setting present in user defaults
        let settings = UserDefaults.standard.bool(forKey: Constants.KEY_SETTINGS)
        btnDelete.isHidden = !settings
        btnMqtt.isHidden = !settings
        btnConnect.isEnabled = !settings

        if (getWiFiSsid() == Constants.SSID) {
            connected = true
            btnConnect.setTitle(Constants.CONNECTED, for: .normal)
            lblInfo.isHidden = settings
        } else {
            btnConnect.setTitle(Constants.CONNECT, for: .normal)
        }
    }

    func getWiFiSsid() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    break
                }
            }
        }
        return ssid
    }

    @IBAction func connect(_ sender: Any) {
        if (connected) {
            self.performSegue(withIdentifier: "GoToWifi", sender: nil)
        } else {
            self.btnConnect.setTitle(Constants.CONNECTING, for: .normal)
            self.loading.startAnimating()

            // connect to WiFi
            let configuration = NEHotspotConfiguration.init(ssid: Constants.SSID, passphrase: Constants.PASSWORD, isWEP: false)
            configuration.joinOnce = true

            NEHotspotConfigurationManager.shared.apply(configuration) { (error) in
                if error != nil {
                    if (error?.localizedDescription == "already associated.") {
                        print("Connected")
                        self.connected = true
                    } else {
                        print("Not Connected")
                        self.connected = false
                    }
                } else {
                    print("Connected")
                    self.connected = true
                }

                self.loading.stopAnimating()
                if (self.connected) {
                    self.btnConnect.setTitle(Constants.CONNECTED, for: .normal)
                    // push to WifiTableViewController to show available hotspots from NodeMCU
                    self.performSegue(withIdentifier: "GoToWifi", sender: nil)
                } else {
                    self.btnConnect.setTitle(Constants.CONNECT, for: .normal)
                }
            }
        }
    }

    @IBAction func deleteSettings(_ sender: Any) {
        let alertController: UIAlertController = UIAlertController(title: "Delete Settings", message: "All settings will be deleted. Do you wish to continue?", preferredStyle: .alert)

        // cancel button
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in

        }
        alertController.addAction(cancelAction)

        //Create an optional action
        let deleteAction: UIAlertAction = UIAlertAction(title: "Delete", style: .destructive) { action -> Void in
            // delete API
            Api.deleteSettings(success: {
                // clear user defaults
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)

                // hide delete button
                self.btnDelete.isHidden = true
                self.btnMqtt.isHidden = true
                self.btnConnect.isEnabled = true

                if (self.getWiFiSsid() == Constants.SSID) {
                    self.btnConnect.setTitle(Constants.CONNECTED, for: .normal)
                    self.lblInfo.isHidden = false
                } else {
                    self.btnConnect.setTitle(Constants.CONNECT, for: .normal)
                    self.lblInfo.isHidden = true
                }
            }, fail: { (error) in
                    let snackbar = TTGSnackbar(message: "Unable to delete settings", duration: .middle, actionText: "OK",
                        actionBlock: { (snackbar) in
                            snackbar.dismiss()
                        })
                    snackbar.contentInset = UIEdgeInsets.init(top: 0, left: 16, bottom: 0, right: 16)
                    snackbar.bottomMargin = CGFloat(64)
                    snackbar.shouldDismissOnSwipe = true
                    snackbar.show()
                })
        }
        alertController.addAction(deleteAction)

        //Present the AlertController
        present(alertController, animated: true, completion: nil)
    }

    @IBAction func mqtt(_ sender: Any) {

        let transport = MQTTCFSocketTransport.init()
        transport.host = UserDefaults.standard.string(forKey: Constants.KEY_MQTT_HOST)
        transport.port = UInt32(UserDefaults.standard.integer(forKey: Constants.KEY_MQTT_PORT))

        let session = MQTTSession.init()
        session?.protocolLevel = MQTTProtocolVersion.version311
        session?.transport = transport
        session?.userName = UserDefaults.standard.string(forKey: Constants.KEY_MQTT_USERNAME)
        session?.password = UserDefaults.standard.string(forKey: Constants.KEY_MQTT_PASSWORD)

        session?.connect(connectHandler: { (error) in
            if (error == nil) {
                session?.publishData(
                    "hello".data(using: .utf8),
                    onTopic: "ESP0001/mufaddal",
                    retain: false,
                    qos: MQTTQosLevel.atLeastOnce)
            } else {
                let snackbar = TTGSnackbar(message: "Couldn't connect to Mqtt", duration: .middle, actionText: "OK",
                    actionBlock: { (snackbar) in
                        snackbar.dismiss()
                    })
                snackbar.contentInset = UIEdgeInsets.init(top: 0, left: 16, bottom: 0, right: 16)
                snackbar.bottomMargin = CGFloat(64)
                snackbar.shouldDismissOnSwipe = true
                snackbar.show()
            }
        })
    }
}
