//
//  WifiTableViewCell.swift
//  nodemcu
//
//  Created by Mufaddal Gulshan on 06/03/19.
//  Copyright © 2019 Saif Limited. All rights reserved.
//

import UIKit

class WifiTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        loading.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        accessoryType = selected ? .checkmark : .none
    }

}
