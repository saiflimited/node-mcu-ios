//
//  WifiTableViewController.swift
//  nodemcu
//
//  Created by Mufaddal Gulshan on 06/03/19.
//  Copyright © 2019 Saif Limited. All rights reserved.
//

import UIKit
import TTGSnackbar

class WifiTableViewController: UITableViewController {

    var hotspots = [String]()
    var searchNetworks: UIActivityIndicatorView?
    var selectedRow = -1
    var joined = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem

        searchNetworks = UIActivityIndicatorView.init(frame: CGRect.init(x: 180.0, y: 38.0, width: 0.0, height: 0.0))
        searchNetworks?.hidesWhenStopped = true
        searchNetworks?.style = .gray

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadHotspots), for: UIControl.Event.valueChanged)
        self.refreshControl = refreshControl

        loadHotspots()
    }

    @objc func loadHotspots() {
        searchNetworks!.startAnimating()
        self.hotspots.removeAll()
        self.tableView.reloadData()

        Api.getHotspots(success: { (SSIDList) in
            self.hotspots.append(contentsOf: SSIDList.ssids)
            self.tableView.reloadData()
            self.searchNetworks!.stopAnimating()
        }) { (error) in
            print(error)
        }
        refreshControl?.endRefreshing()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return hotspots.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WifiCell", for: indexPath) as! WifiTableViewCell

        // Configure the cell...
        cell.name?.text = hotspots[indexPath.row]
        cell.loading.stopAnimating()
        cell.loading.isHidden = true

        if (selectedRow == indexPath.row) {
            if(joined) {
                cell.accessoryType = .checkmark
            }
            else {
                cell.loading.startAnimating()
                cell.loading.isHidden = false
            }
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertController: UIAlertController = UIAlertController(title: "Enter Password", message: "Enter password to join " + hotspots[tableView.indexPathForSelectedRow?.row ?? 0], preferredStyle: .alert)

        //cancel button
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //cancel code
            self.selectedRow = -1
            tableView.reloadData()
        }
        alertController.addAction(cancelAction)

        //Create an optional action
        let nextAction: UIAlertAction = UIAlertAction(title: "Join", style: .default) { action -> Void in
            // join network
            let connect = Connect()
            connect.username = self.hotspots[indexPath.row]
            connect.password = ((alertController.textFields?.first)?.text!)!
            self.selectedRow = indexPath.row
            tableView.reloadData()
            Api.connectWifi(connect: connect, success: {
                // success
                self.joined = true
                UserDefaults.standard.set(true, forKey: Constants.KEY_SETTINGS)

                Api.getMqtt(success: { (mqtt) in
                    UserDefaults.standard.set(mqtt.username, forKey: Constants.KEY_MQTT_USERNAME)
                    UserDefaults.standard.set(mqtt.password, forKey: Constants.KEY_MQTT_PASSWORD)
                    UserDefaults.standard.set(mqtt.host, forKey: Constants.KEY_MQTT_HOST)
                    UserDefaults.standard.set(mqtt.port, forKey: Constants.KEY_MQTT_PORT)
                    tableView.reloadData()
                }, fail: { (error) in
                        let snackbar = TTGSnackbar(message: "Unable to fetch Mqtt details", duration: .middle, actionText: "OK",
                            actionBlock: { (snackbar) in
                                snackbar.dismiss()
                            })
                        snackbar.contentInset = UIEdgeInsets.init(top: 0, left: 16, bottom: 0, right: 16)
                        snackbar.bottomMargin = CGFloat(64)
                        snackbar.shouldDismissOnSwipe = true
                        snackbar.show()
                    })
            }, fail: { (error) in
                    self.selectedRow = -1
                    print(error)
                    // fail
                    let snackbar = TTGSnackbar(message: "Couldn't join WiFi", duration: .middle, actionText: "OK",
                        actionBlock: { (snackbar) in
                            snackbar.dismiss()
                        })
                    snackbar.contentInset = UIEdgeInsets.init(top: 0, left: 16, bottom: 0, right: 16)
                    snackbar.bottomMargin = CGFloat(64)
                    snackbar.shouldDismissOnSwipe = true
                    snackbar.show()
                    tableView.reloadData()
                })
        }
        alertController.addAction(nextAction)

        //Add text field
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Enter Password"
            textField.isSecureTextEntry = true
            textField.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        })

        alertController.actions[1].isEnabled = false

        //Present the AlertController
        present(alertController, animated: true, completion: nil)

        // deselect
        tableView.deselectRow(at: indexPath, animated: false)
    }

    @objc func textChanged(_ sender: Any) {
        let tf = sender as! UITextField
        var resp: UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        alert.actions[1].isEnabled = (tf.text != "")
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0))
        let headerLabel: UILabel = UILabel.init(frame: CGRect.init(x: 20.0, y: 30.0, width: 320.0, height: 16.0))

        headerLabel.backgroundColor = tableView.backgroundColor
        headerLabel.text = "CHOOSE A NETWORK..."
        headerLabel.font = UIFont.systemFont(ofSize: 13.0)
        headerLabel.textColor = UIColor.init(red: 61.0 / 255.0, green: 77.0 / 255.0, blue: 99.0 / 255.0, alpha: 1.0)
        headerLabel.shadowColor = UIColor.init(white: 1.0, alpha: 0.65)
        headerLabel.shadowOffset = CGSize.init(width: 0.0, height: 1.0)

        headerView.addSubview(headerLabel)
        headerView.addSubview(searchNetworks!)
        return headerView
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let nc = segue.destination as! UINavigationController
        let vc = nc.viewControllers.first as! PasswordTableViewController
        vc.hotspotSSID = hotspots[tableView.indexPathForSelectedRow?.row ?? 0]
    }
}
