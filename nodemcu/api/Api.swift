//
//  API.swift
//  Salon
//
//  Created by Mufaddal Gulshan on 19/10/18.
//  Copyright © 2018 saiflimited. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON
import MKProgress
import TTGSnackbar

class Api: NSObject {

//    static let ROOT = "http://localhost:3012"
//    static let ROOT = "http://192.168.1.9:3012"
//    static let ROOT = "http://192.168.43.53:3012"
    // static let ROOT = "http://192.168.0.197:3012"
    static let ROOT = "http://192.168.4.1"

    // user
    static let UPDATE_USER_LOCATIONS = ROOT + "/users/locations"

    static var sessionManager: SessionManager {
        get {
            let sm = Alamofire.SessionManager.default
//            sm.adapter = GlamoHeadersAdapter()
            return sm
        }
    }

    static func checkNetwork(call: @escaping () -> Void) -> Void {
        if (NetworkReachabilityManager()!.isReachable) {
            call()
        }
        else {
            let snackbar = TTGSnackbar(message: "Unable to reach our servers!", duration: .forever, actionText: "OK",
                actionBlock: { (snackbar) in
                    snackbar.dismiss()
                })
            snackbar.contentInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
            snackbar.bottomMargin = CGFloat(64)
            snackbar.shouldDismissOnSwipe = true
            snackbar.show()
        }
    }

    static func getHotspots(success: @escaping (_ SSIDs: SSIDList) -> Void, fail: @escaping (_ error: NSError) -> Void) -> Void {
        checkNetwork {
            sessionManager.request(ROOT + "/list", method: .get, encoding: JSONEncoding.default)
                .validate()
                .responseObject { (response: DataResponse<SSIDList>) in
                    switch response.result {
                    case .success(let list):
                        success(list)
                    case .failure(let error):
                        fail(error as NSError)
                    }
            }
        }
    }

    static func connectWifi(connect: Connect, success: @escaping () -> Void, fail: @escaping (_ error: NSError) -> Void) -> Void {
        checkNetwork {
            sessionManager.request(ROOT + "/connectwifi", method: .post, parameters: connect.toJSON(), encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        success()
                    case .failure(let error):
                        fail(error as NSError)
                    }
            }
        }
    }

    static func getMqtt(success: @escaping (_ mqtt: Mqtt) -> Void, fail: @escaping (_ error: NSError) -> Void) -> Void {
        checkNetwork {
            sessionManager.request(ROOT + "/mqtt", method: .get, encoding: JSONEncoding.default)
                .validate()
                .responseObject { (response: DataResponse<Mqtt>) in
                    switch response.result {
                    case .success(let mqtt):
                        success(mqtt)
                    case .failure(let error):
                        fail(error as NSError)
                    }
            }
        }
    }
    
    static func deleteSettings(success: @escaping () -> Void, fail: @escaping (_ error: NSError) -> Void) -> Void {
        checkNetwork {
            sessionManager.request(ROOT + "/delete", method: .get, encoding: JSONEncoding.default)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        success()
                    case .failure(let error):
                        fail(error as NSError)
                    }
            }
        }
    }
}
