//
//  Location.swift
//  Salon
//
//  Created by Mufaddal Gulshan on 18/10/18.
//  Copyright © 2018 saiflimited. All rights reserved.
//

import Foundation
import ObjectMapper

class Mqtt: Mappable {

    var username = ""
    var password = ""
    var host = ""
    var port = 0

    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        username <- map["username"]
        password <- map["password"]
        host <- map["host"]
        port <- map["port"]
    }
}
