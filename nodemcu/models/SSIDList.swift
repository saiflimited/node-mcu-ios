//
//  File.swift
//  Salon
//
//  Created by Mufaddal Gulshan on 18/10/18.
//  Copyright © 2018 saiflimited. All rights reserved.
//

import Foundation
import ObjectMapper

class SSIDList: Mappable {

    var ssids = [String]()
    
    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        ssids <- map["ssids"]
    }
}
