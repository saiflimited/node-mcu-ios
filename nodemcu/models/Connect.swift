//
//  Order.swift
//  Salon
//
//  Created by Mufaddal Gulshan on 18/10/18.
//  Copyright © 2018 saiflimited. All rights reserved.
//

import Foundation
import ObjectMapper

class Connect: Mappable {

    var username = ""
    var password = ""

    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        username <- map["username"]
        password <- map["password"]
    }
}
